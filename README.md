# VPN





VPN隧道即虚拟专用网隧道。

它是通过一个**公共网络（通常是因特网）**与**企业内部的网络**建立一个**临时的、安全的连接**，使VPN隧道的使用者**临时成为企业内部网络的一部分**。

因此VPN隧道是对企业内部网的扩展，通过它可以帮助远程用户、企业支机构与企业的内部网建立可信的安全连接，并保证数据的安全传输。

随着日益频繁国内、国际学术交流的发展，以及人员居住环境的变化。不少人员身居组织外部甚至在国外，他们需要利用公共网络访问校园网。

而不少组织内部的资源 （例如图书馆电子图书服务、学校购买的国外学术资源、内部信息资源等）只对校内的IP地址开放。

**VPN隧道服务可使在公共网络上的组织用户临时成为组织内网的一部分，以使用户可方便地访问各种需要授权访问的网络资源。**





------



## **ocserv**

ocserv 全称是 OpenConnect VPN Server。是一个VPN服务器软件，实现了 Cisco 的 AnyConnect SSL VPN 协议，兼容 OpenConnection VPN 客户端。

特点是体积小、安全和可配置。依赖标准协议如 TLS 1.2 和数据报文 TLS。它通过实现Cisco的AnyConnect协议，用DTLS作为主要的加密传输协议。

OpenConnect 原本是由于 AnyConnect 有只能运行在 Cisco 设备上限制而开发出来的一个多系统支持的开源 VPN 实现方式，属于 SSL VPN，需要一个有效的 SSL 证书。

它的主要好处在于:

- AnyConnect的VPN协议默认使用UDP DTLS作为数据传输，但如果有什么网络问题导致UDP传输出现问题，它会利用最初建立的TCP TLS通道作为备份通道，降低VPN断开的概率。

- AnyConnect作为Cisco新一代的VPN解决方案，被用于许多大型企业，这些企业依赖它提供正常的商业运作。

- AnyConnect的假设比OpenConnet简单得多，运维成本低，而且支持全平台客户端。

  

AnyConnect作为Cisco专有技术，其服务端只能运行在Cisco设备上，即如果没有购买Cisco相关设备，将无法使用AnyConnect服务端。

而OpenConnect(ocserv)的出现解决了这一个问题，OpenConnect是一个开源项目，其目标是在相对廉价的linux设备上运行与AnyConnect协议兼容的服务端，以此来使用该协议而不需要购买Cisco专有设备。 

AnyConnect目前支持 Windows 7+ / Android / IOS / Mac ，其他设备没有客户端所以无法使用，例如 XP系统。

ocserv 是一个 openconnect  server 的容器化镜像。OpenConnect Server是一个 SSL VPN服务器软件，它实现了cisco公司的OpenConnect SSL VPN 协议 。
同时也兼容使用 AnyConnect SSL VPN 协议的客户端。 

它可以使用两种 VPN 连接模式（Route/ALL）：

- Route方式连接后，只有符合策略的请求走VPN。

- ALL方式连接后，默认所有的连接都走VPN。





# 安装和部署



网上许多方法都是通过手动编译源代码包的方式安装，然而现在至少对于 Debian 系的系统来说已经有了编译好的软件包了，详情见 [Distribution Status](https://ocserv.gitlab.io/www/packages.html)，

对于 Debian 系服务器来说（比如本例的 Ubuntu）直接一条指令即可（非常感谢维护这个包的：Aron Xu，Liang Guo 和 Mike Miller）

```shell
# 安装
apt-get update
apt-get install -y ocserv

# 打开系统的转发功能，在 /etc/sysctl.conf 中追加入如下行：
echo "net.ipv4.ip_forward=1"  >> /etc/sysctl.conf
```





